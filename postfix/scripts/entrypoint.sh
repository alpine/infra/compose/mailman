#!/bin/sh

set -exo pipefail

postconf -ve maillog_file=/dev/stdout
postconf -ve recipient_delimiter=+
postconf -ve owner_request_special=no
postconf -ve unknown_local_recipient_reject_code=550
postconf -ve mynetworks=10.0.0.0/8,172.16.0.0/12,192.168.0.0/16

if [ "$POSTFIX_MYHOSTNAME" ]; then
	postconf -ve myhostname="$POSTFIX_MYHOSTNAME"
fi

postconf -ve transport_maps=regexp:/srv/mailman/var/data/postfix_lmtp
postconf -ve local_recipient_maps=regexp:/srv/mailman/var/data/postfix_lmtp
postconf -ve relay_domains=regexp:/srv/mailman/var/data/postfix_domains

newaliases

exec $@
