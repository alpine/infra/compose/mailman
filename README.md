# Alpine Linux mailman dockerized

## Usage

Add an `.env` file in the root of this project with following variables

```sh
POSTGRES_PASSWORD=mypostgrespassword  
MAILMAN_SECRET_KEY=djangosecretkey  
MAILMAN_ARCHIVER_KEY=hyperkittysecretkey  
POSTFIX_MYHOSTNAME=postfix.host.name  
```

```sh
$ docker-compose up -d
```
