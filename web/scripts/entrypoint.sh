#!/bin/sh

set -euxo pipefail

# wait until postgres is ready
while ! pg_isready -qh postgres; do sleep 1; done

export MAILMAN_CORE_IP=$(getent hosts core | cut -d' ' -f1)

# init or upgrade db if needed
./manage.py migrate

# copy static files to mount point
./manage.py collectstatic --noinput --clear --verbosity 0
./manage.py compress --force --verbosity 0 

chown -R mailman:mailman /var/lib/webapps/mailman

exec $@
