#!/bin/sh

set -euxo pipefail

# wait until postgres is ready
while ! pg_isready -qh postgres; do sleep 1; done

# get mailman core ip address as mailman is on drugs.
MAILMAN_CORE_IP=$(getent hosts core | cut -d' ' -f1)

sed -e "s/%%MAILMAN_CORE_IP%%/$MAILMAN_CORE_IP/g" \
	-e "s/%%POSTGRES_PASSWORD%%/$POSTGRES_PASSWORD/g" \
	/etc/mailman/mailman.cfg.tpl > \
	/etc/mailman/mailman.cfg

sed -e "s/%%MAILMAN_ARCHIVER_KEY%%/$MAILMAN_ARCHIVER_KEY/g" \
	/etc/mailman/mailman-hyperkitty.cfg.tpl > \
	/etc/mailman/mailman-hyperkitty.cfg

mailman aliases

chown -R mailman /srv/mailman

su-exec mailman $@