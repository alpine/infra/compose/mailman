# https://docs.mailman3.org/projects/mailman/en/latest/src/mailman/config/docs/config.html#mailman-cfg

[database]
class: mailman.database.postgresql.PostgreSQLDatabase
url: postgres://mailman:%%POSTGRES_PASSWORD%%@postgres/mailman

[mta]
lmtp_host: %%MAILMAN_CORE_IP%%
smtp_host: postfix
configuration: /etc/mailman/mailman-postfix.cfg

[archiver.hyperkitty]
class: mailman_hyperkitty.Archiver
enable: yes
configuration: /etc/mailman/mailman-hyperkitty.cfg

[webservice]
hostname: %%MAILMAN_CORE_IP%%