# https://gitlab.com/mailman/mailman-hyperkitty/blob/master/mailman-hyperkitty.cfg

[general]

# This is your HyperKitty installation, preferably on the localhost. This
# address will be used by Mailman to forward incoming emails to HyperKitty
# for archiving. It does not need to be publicly available, in fact it's
# better if it is not.
# However, if your Mailman installation is accessed via HTTPS, the URL needs
# to match your SSL certificate (e.g. https://lists.example.com/hyperkitty).
base_url: http://web:8080/hyperkitty/

# Shared API key, must be the identical to the value in HyperKitty's
# settings.
api_key: %%MAILMAN_ARCHIVER_KEY%%
